import Lhform from '../src/';

const form = document.forms.myForm;
const myForm = new Lhform(form, false,
  {
    'file[]': {
      required: 'Это поле обязательно',
      accept: 'Выберите изображение',
      minlength: 'Выберите больше 1 файла',
      maxlength: 'Выберите меньше 5 файлов',
      minfilesize: 'Слишком маленький файл',
      maxfilesize: 'Слишком большой файл',
    },
  },
  {
    dataFn: () => (
      [
        { name: 'jio', value: 13 },
      ]
    ),
    successCb: data => console.log(data),
    errorCb: (a, b) => console.log(a, b),
  },
);
myForm.init();
