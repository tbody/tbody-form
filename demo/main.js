import Tbodyform from '../dist/index.min';

const form = document.forms.myForm;

const myForm = new Tbodyform({
  node: form,
  messages: {
    name: {
      required: 'Это поле обязательно',
      minlength: 'Минимум 2 символа',
      maxlength: 'Максимум 10 символов',
    },
    email: {
      required: 'Это поле обязательно',
      pattern: 'Введите корректный email',
    },
    age: {
      required: 'Это поле обязательно',
      min: 'Минимум 18',
      max: 'Максимум 56',
    },
    password: {
      required: 'Это поле обязательно',
    },
    password2: {
      equalTo: 'Пароли должны совпадать',
    },
    'theme[]': {
      required: 'Это поле обязательно',
      minlength: 'Минимум 2 темы',
      maxlength: 'Максимум 3 темы',
    },
    comment: {
      required: 'Это поле обязательно',
      minlength: 'Минимум 10 символов',
      maxlength: 'Максимум 50 символов',
    },
    language: {
      required: 'Выберите язык',
    },
    'color[]': {
      required: 'Выберите цвета',
      minlength: 'Минимум 2 цвета',
      maxlength: 'Максимум 3 цвета',
    },
    'image[]': {
      required: 'Это поле обязательно',
      accept: 'Выберите изображение',
      minlength: 'Выберите больше 1 файла',
      maxlength: 'Выберите меньше 5 файлов',
      minfilesize: 'Слишком маленький файл',
      maxfilesize: 'Слишком большой файл',
    },
  },
  isMultistep: true,
  dataFn: () => (
    [
      { name: 'jio', value: 13 },
    ]
  ),
  beforeSend: formIns => console.log(formIns),
  successCb: (formIns, data) => console.log(formIns, data),
  errorCb: (a, b) => console.log(a, b),
});
myForm.init();
