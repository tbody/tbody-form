const webpack = require('webpack');
const path = require('path');


const config = {
  entry: {
    index: './src/index',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].min.js',
    library: 'Tbodyform',
    libraryTarget: 'umd',
  },
  module: {
    rules: [
      {
        test: /\.js/,
        exclude: [
          path.resolve(__dirname, 'node_modules'),
        ],
        loader: 'babel-loader',
      },
    ],
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      mangle: true,
    }),
  ],
};

module.exports = config;