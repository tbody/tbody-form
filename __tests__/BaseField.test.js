import { CLASS_NAMES } from '../src/constants';
import BaseField from '../src/constructors/BaseField';


test('Base fields', () => {
  const div = document.createElement('div');
  div.classList.add(CLASS_NAMES.fieldContainer);

  const inputText = document.createElement('input');
  inputText.type = 'text';
  inputText.name = 'name';
  inputText.id = 'name';

  const error = document.createElement('label');
  error.setAttribute('for', 'name');
  error.classList.add(CLASS_NAMES.error);
  error.textContent = 'Минимум 3 символа';

  div.appendChild(inputText);

  const divWithError = div.cloneNode(true);
  divWithError.classList.add(CLASS_NAMES.hasError);
  divWithError.appendChild(error);

  const eventChange = document.createEvent('HTMLEvents');
  eventChange.initEvent('change', false, true);

  const eventKeyup = document.createEvent('HTMLEvents');
  eventKeyup.initEvent('keyup', false, true);

  const settings = {
    rules: {
      required: true,
      minlength: 3,
    },
    messages: {
      required: 'Это поле обязательно',
      minlength: 'Минимум 3 символа',
    },
  };

  const field = new BaseField(0, 'name', inputText, 'text', settings);

  inputText.value = 'aaa';
  inputText.dispatchEvent(eventChange);
  expect(field.isDirty).toBeFalsy();
  expect(field.getValue()).toEqual({ name: 'name', value: 'aaa' });
  expect(field.isDirty).toBeFalsy();
  expect(field.validate()).toBeTruthy();
  expect(field.isDirty).toBeTruthy();

  inputText.value = 'bb';
  inputText.dispatchEvent(eventKeyup);
  expect(field.getValue()).toEqual({ name: 'name', value: 'bb' });

  divWithError.getElementsByTagName('input')[0].classList.add(CLASS_NAMES.nonvalid);
  expect(div).toEqual(divWithError);
});
