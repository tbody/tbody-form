import validateByMethodsNames, {
  changeStatusValidation,
  getStatusValidationFields,
  validateRequired,
  validateLength,
  validateByPattern,
  validateNumber,
  validateEqualTo,
} from '../src/validation';


test('Get status validation from fields DOM element', () => {
  const field = {
    getStatus() {
      return true;
    },
  };
  const field2 = {
    getStatus() {
      return false;
    },
  };

  expect(getStatusValidationFields([field, field])).toBeTruthy();
  expect(getStatusValidationFields([field, field2])).toBeFalsy();
});

test('Change status validation from fields DOM element', () => {
  const field = document.createElement('input');
  field.classList.add('js-valid');
  const field2 = field.cloneNode(true);

  const field3 = field.cloneNode(true);
  const field4 = field.cloneNode(true);

  const fieldWithError = document.createElement('input');
  fieldWithError.classList.add('js-nonvalid');
  const fieldWithError2 = fieldWithError.cloneNode(true);

  changeStatusValidation([field, field2], false);
  expect([field, field2]).toEqual([fieldWithError, fieldWithError2]);

  changeStatusValidation([field, field2], true);
  expect([field, field2]).toEqual([field3, field4]);
});

test('Validate required', () => {
  expect(validateRequired('')).toEqual({ type: 'required', isValid: false });
  expect(validateRequired('a')).toEqual({ type: 'required', isValid: true });
  expect(validateRequired('aa')).toEqual({ type: 'required', isValid: true });
  expect(validateRequired(1)).toEqual({ type: 'required', isValid: true });
  expect(validateRequired(56)).toEqual({ type: 'required', isValid: true });
  expect(validateRequired(0)).toEqual({ type: 'required', isValid: true });
  expect(validateRequired(null)).toEqual({ type: 'required', isValid: false });
  expect(validateRequired(undefined)).toEqual({ type: 'required', isValid: false });
  expect(validateRequired(NaN)).toEqual({ type: 'required', isValid: false });
  expect(validateRequired()).toEqual({ type: 'required', isValid: false });
});

test('Validate minlength', () => {
  expect(() => validateLength('minlength', 3, 3)).toThrowError('This value is not array or string');
  expect(() => validateLength('minlength', 56, 3)).toThrowError('This value is not array or string');
  expect(() => validateLength('minlength', {}, 3)).toThrowError('This value is not array or string');
  expect(validateLength('minlength', 'aa', 3)).toEqual({ type: 'minlength', isValid: false });
  expect(validateLength('minlength', 'aaa', 3)).toEqual({ type: 'minlength', isValid: true });
  expect(validateLength('minlength', 'aaaa', 3)).toEqual({ type: 'minlength', isValid: true });
  expect(validateLength('minlength', [1], 3)).toEqual({ type: 'minlength', isValid: false });
  expect(validateLength('minlength', [1, 1, 1], 3)).toEqual({ type: 'minlength', isValid: true });
  expect(validateLength('minlength', [1, 1, 1, 1], 3)).toEqual({ type: 'minlength', isValid: true });
});

test('Validate maxlength', () => {
  expect(() => validateLength('maxlength', 3, 3)).toThrowError('This value is not array or string');
  expect(() => validateLength('maxlength', 56, 3)).toThrowError('This value is not array or string');
  expect(() => validateLength('maxlength', {}, 3)).toThrowError('This value is not array or string');
  expect(validateLength('maxlength', 'aa', 3)).toEqual({ type: 'maxlength', isValid: true });
  expect(validateLength('maxlength', 'aaa', 3)).toEqual({ type: 'maxlength', isValid: true });
  expect(validateLength('maxlength', 'aaaa', 3)).toEqual({ type: 'maxlength', isValid: false });
  expect(validateLength('maxlength', [1], 3)).toEqual({ type: 'maxlength', isValid: true });
  expect(validateLength('maxlength', [1, 1, 1], 3)).toEqual({ type: 'maxlength', isValid: true });
  expect(validateLength('maxlength', [1, 1, 1, 1], 3)).toEqual({ type: 'maxlength', isValid: false });
});


test('Validate by pattern', () => {
  expect(() => validateByPattern(56, /^hello/)).toThrowError('This value is not string');
  expect(() => validateByPattern({}, /^hello/)).toThrowError('This value is not string');
  expect(validateByPattern('aa', /^hello/)).toEqual({ type: 'pattern', isValid: false });
  expect(validateByPattern('hello world!', /^hello/)).toEqual({ type: 'pattern', isValid: true });
});

test('Validate min', () => {
  expect(() => validateNumber('min', '56', 10)).toThrowError('This value is not number');
  expect(() => validateNumber('min', NaN, 10)).toThrowError('This value is not number');
  expect(() => validateNumber('min', 'aa', 10)).toThrowError('This value is not number');
  expect(() => validateNumber('min', {}, 10)).toThrowError('This value is not number');
  expect(() => validateNumber('min', true, 10)).toThrowError('This value is not number');
  expect(validateNumber('min', 5, 10)).toEqual({ type: 'min', isValid: false });
  expect(validateNumber('min', 10, 10)).toEqual({ type: 'min', isValid: true });
  expect(validateNumber('min', 56, 10)).toEqual({ type: 'min', isValid: true });
});

test('Validate max', () => {
  expect(() => validateNumber('max', '56', 10)).toThrowError('This value is not number');
  expect(() => validateNumber('max', NaN, 10)).toThrowError('This value is not number');
  expect(() => validateNumber('max', 'aa', 10)).toThrowError('This value is not number');
  expect(() => validateNumber('max', {}, 10)).toThrowError('This value is not number');
  expect(() => validateNumber('max', true, 10)).toThrowError('This value is not number');
  expect(validateNumber('max', 5, 10)).toEqual({ type: 'max', isValid: true });
  expect(validateNumber('max', 10, 10)).toEqual({ type: 'max', isValid: true });
  expect(validateNumber('max', 56, 10)).toEqual({ type: 'max', isValid: false });
});

test('Validate equal to', () => {
  expect(() => validateEqualTo({}, 10)).toThrowError('It not admissible comparison');
  expect(() => validateEqualTo(NaN, NaN)).toThrowError('It not admissible comparison');
  expect(() => validateEqualTo(NaN, 10)).toThrowError('It not admissible comparison');
  expect(() => validateEqualTo({}, 10)).toThrowError('It not admissible comparison');
  expect(() => validateEqualTo(true, true)).toThrowError('It not admissible comparison');
  expect(() => validateEqualTo([1, 1], 1)).toThrowError('It not admissible comparison');
  expect(validateEqualTo(5, 10)).toEqual({ type: 'equalTo', isValid: false });
  expect(validateEqualTo(5, null)).toEqual({ type: 'equalTo', isValid: false });
  expect(validateEqualTo(null, 10)).toEqual({ type: 'equalTo', isValid: false });
  expect(validateEqualTo(10, 10)).toEqual({ type: 'equalTo', isValid: true });
  expect(validateEqualTo('aa', 10)).toEqual({ type: 'equalTo', isValid: false });
  expect(validateEqualTo('aa', 'aa')).toEqual({ type: 'equalTo', isValid: true });
  expect(validateEqualTo('aa', 'aaa')).toEqual({ type: 'equalTo', isValid: false });
  expect(validateEqualTo([1, 1], [1, 1, 1])).toEqual({ type: 'equalTo', isValid: false });
  expect(validateEqualTo([1, 1], [1, 1])).toEqual({ type: 'equalTo', isValid: true });
  expect(validateEqualTo(['aa', 1], [1, 1])).toEqual({ type: 'equalTo', isValid: false });
  expect(validateEqualTo(['aa', 'bb'], ['aa', 'bb'])).toEqual({ type: 'equalTo', isValid: true });
  expect(validateEqualTo(['aa', 'bb'], ['aa', 'bbb'])).toEqual({ type: 'equalTo', isValid: false });
  expect(validateEqualTo(['aa', 'bb'], ['aa', 'bb', 'cc'])).toEqual({ type: 'equalTo', isValid: false });
});

test('Validate by names of methods', () => {
  expect(validateByMethodsNames({
    required: true,
    minlength: 4,
    maxlength: 10,
  }, 'aa')).toEqual({ type: 'minlength', isValid: false });

  expect(validateByMethodsNames({
    required: true,
    maxlength: 10,
  }, 'aaff')).toEqual({ isValid: true });

  expect(validateByMethodsNames({
    required: true,
    minlength: 4,
    pattern: /^hello/,
    maxlength: 10,
  }, 'aaf')).toEqual({ type: 'pattern', isValid: false });

  expect(validateByMethodsNames({
    required: true,
    minlength: 4,
    pattern: /^hello/,
    maxlength: 15,
    equalTo: 'hello world!',
  }, 'hello world!')).toEqual({ isValid: true });
});
