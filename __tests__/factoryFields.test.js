import factoryFields from '../src/factoryFields';
import BaseField from '../src/constructors/BaseField';
import HiddenField from '../src/constructors/HiddenField';
import { hasProp } from '../src/utils';


test('Factory fields', () => {
  const form = document.createElement('form');
  form.innerHTML = `
    <input type="text" name="name" required>
    <input type="hidden" name="hidden">
  `;

  const container = new (function Container() {
    this.subscribers = {};
    this.subscribe = (fn, type) => {
      if (!hasProp(this.subscribers, type)) {
        this.subscribers[type] = [];
      }
      this.subscribers[type].push(fn);
    };
    this.DOM = form;
  })();

  const messages = {
    name: {
      required: 'Это поле обязательно',
    },
  };

  const fields = factoryFields(container, messages);

  expect(fields[0]).toBeInstanceOf(BaseField);
  expect(fields[1]).toBeInstanceOf(HiddenField);
  expect(fields[0].messages).toEqual(messages.name);
});
