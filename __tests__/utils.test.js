import Observable from '../src/constructors/Observable';
import {
  EVENTS_NAMES,
} from '../src/constants';

import {
  isNumberOrString,
  hasProp,
  isArraysToEqual,
  filterArrayOfObjectsByKey,
  subscribeOneToMany,
  subscribeToEqualField,
  createFileUniqueId,
  formatBytesToString,
  createFormData,
} from '../src/utils';

test('Is number or string', () => {
  expect(isNumberOrString(NaN)).toBeFalsy();
  expect(isNumberOrString([])).toBeFalsy();
  expect(isNumberOrString([1, 1])).toBeFalsy();
  expect(isNumberOrString({})).toBeFalsy();
  expect(isNumberOrString({ a: 1 })).toBeFalsy();
  expect(isNumberOrString()).toBeFalsy();
  expect(isNumberOrString('a')).toBeTruthy();
  expect(isNumberOrString(1)).toBeTruthy();
});

test('Object has property', () => {
  const obj = {
    a: 1,
    b: 2,
    56: 'ololo',
  };

  expect(hasProp(obj, 1)).toBeFalsy();
  expect(hasProp(obj, name)).toBeFalsy();
  expect(hasProp(obj)).toBeFalsy();
  expect(hasProp()).toBeFalsy();
  expect(hasProp(obj, 'a')).toBeTruthy();
  expect(hasProp(obj, 56)).toBeTruthy();
});

test('Is array to equal', () => {
  expect(() => isArraysToEqual([{ a: 1 }, 1], [1, 1])).toThrowError('It not admissible comparison');
  expect(() => isArraysToEqual([{ a: 1 }, 1], [{ a: 1 }, 1])).toThrowError('It not admissible comparison');
  expect(() => isArraysToEqual([1, undefined], [1, 1])).toThrowError('It not admissible comparison');
  expect(isArraysToEqual([], [1, 1])).toBeFalsy();
  expect(isArraysToEqual([1], [1, 1])).toBeFalsy();
  expect(isArraysToEqual([1, 2], [1, 2])).toBeTruthy();
  expect(isArraysToEqual(['aa', 'bb'], ['aa', 'bb'])).toBeTruthy();
});

test('Filter array of objects by keys object', () => {
  expect(filterArrayOfObjectsByKey(
    [
      { name: 'required', a: '33' },
      { name: 'pattern', b: 3 },
      { name: 'minlength', c: null },
      { name: 'maxlength', d: {} },
    ],
    {
      required: true,
      minlength: 2,
    },
    'name',
  ))
    .toEqual([
      { name: 'required', a: '33' },
      { name: 'minlength', c: null },
    ]);
});

test('Subsribe one to many', () => {
  const observable = new Observable();
  const observable2 = new Observable();

  const foo = function foo(a) {
    console.log(a);
  };

  subscribeOneToMany(foo, [observable, observable2], 'any');
  expect(observable.subscribers.any).toEqual([foo]);
  expect(observable2.subscribers.any).toEqual([foo]);
});

test('Subsribe to equal field', () => {
  const field = new Observable();
  field.name = 'age';

  const field2 = new Observable();
  field2.name = 'age2';
  field2.setValueEqualField = () => {};
  field2.rules = {
    equalTo: 'age',
  };

  subscribeToEqualField([field, field2]);

  expect(field.subscribers).toEqual({
    [EVENTS_NAMES.changeValue]: [field2.setValueEqualField],
  });
});

test('Create file unique id', () => {
  const file = {
    lastModified: '1',
    name: 'myName',
    size: '10',
  };

  expect(createFileUniqueId(file)).toBe('1myName10');
});

test('Format bytes to string', () => {
  expect(formatBytesToString('1000')).toBe('0.98 KB');
  expect(formatBytesToString('503484')).toBe('491.68 KB');
  expect(formatBytesToString('254994000')).toBe('243.18 MB');
  expect(formatBytesToString('784371848547')).toBe('730.50 GB');
  expect(formatBytesToString('1463052768386')).toBe('1362.57 GB');
});

test('Create form data', () => {
  const fields = [
    {
      getName: () => 'name',
      getValue: () => 'petr',
    },
    {
      getName: () => 'age',
      getValue: () => 34,
    },
    {
      getName: () => 'children[]',
      getValue: () => ['anna', 'kolya'],
    },
  ];

  const expectedData = new FormData();
  expectedData.append(fields[0].getName(), fields[0].getValue());
  expectedData.append(fields[1].getName(), fields[1].getValue());
  expectedData.append(fields[2].getName(), fields[1].getValue()[0]);
  expectedData.append(fields[2].getName(), fields[1].getValue()[1]);

  expect(expectedData).toEqual(createFormData(fields));
});
