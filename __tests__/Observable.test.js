import Observable from '../src/constructors/Observable';

test('Observable consturctor', () => {
  const result = [];
  const observable = new Observable();
  const foo = function foo(a) {
    result.push(a);
  };
  const bar = function bar(a) {
    result.push(a * 3);
  };

  observable.subscribe(foo, 'any');
  observable.subscribe(bar, 'any');
  observable.publish(2, 'any');

  expect(observable.subscribers.any).toEqual([foo, bar]);
  expect(result).toEqual([2, 6]);
});