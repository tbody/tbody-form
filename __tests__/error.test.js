import {
  createError,
  showError,
  hideError,
  toggleFieldError,
} from '../src/errors';

test('Create node error', () => {
  const input = document.createElement('input');
  input.type = 'text';
  input.name = 'name';
  input.id = 'name';

  const error = document.createElement('label');
  error.setAttribute('for', 'name');
  error.textContent = 'Это ошибка';
  error.classList.add('js-error');

  expect(createError(input, 'Это ошибка')).toEqual(error);
});

test('Show error for one node', () => {
  const container = document.createElement('div');
  container.classList.add('container');

  const input = document.createElement('input');
  input.type = 'text';
  input.name = 'name';
  input.id = 'name';

  container.appendChild(input);

  const error = document.createElement('label');
  error.setAttribute('for', 'name');
  error.textContent = 'Это ошибка';
  error.classList.add('js-error');

  const hiddenError = error.cloneNode(true);
  hiddenError.style.display = 'none';

  expect(showError(input, 'Это ошибка', null).error).toEqual(error);
  expect(showError(input, 'Это ошибка', hiddenError).error).toEqual(error);
});

test('Show error on multi fields', () => {
  const container = document.createElement('div');
  container.classList.add('container');
  container.classList.add('js-field-container');

  const inner = document.createElement('div');
  inner.classList.add('inner');

  const input = document.createElement('input');
  input.type = 'radio';
  input.name = 'name';
  input.id = 'name-1';

  const input2 = document.createElement('input');
  input2.type = 'radio';
  input2.name = 'name';
  input2.id = 'name-2';

  container.appendChild(input);
  container.appendChild(inner);
  inner.appendChild(input2);

  const error = document.createElement('label');
  error.setAttribute('for', 'name-1');
  error.textContent = 'Это ошибка';
  error.classList.add('js-error');

  const hiddenError = error.cloneNode(true);
  hiddenError.style.display = 'none';

  const expectedContainer = container.cloneNode(true);
  expectedContainer.classList.add('js-has-error');
  expectedContainer.appendChild(error);

  const result = showError(input, 'Это ошибка', null);

  expect(result.error).toEqual(error);
  expect(container).toEqual(expectedContainer);
});

test('Hide error', () => {
  const container = document.createElement('div');
  container.classList.add('js-field-container');
  container.classList.add('js-has-error');

  const error = document.createElement('label');
  error.setAttribute('for', 'name-1');
  error.textContent = 'Это ошибка';
  error.classList.add('js-error');

  container.appendChild(error);

  const expectedContainer = container.cloneNode(true);
  expectedContainer.classList.remove('js-has-error');
  expectedContainer.querySelector('.js-error')
    .style.display = 'none';

  expect(hideError(error, container).parent).toEqual(expectedContainer);
});

test('Toggle field error', () => {

});
