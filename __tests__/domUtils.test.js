/**
 * @jest-environment jest-environment-jsdom-11.0.0
 */

import {
  CLASS_NAMES,
} from '../src/constants';

import {
  getFormAttributes,
  getFormFieldsets,
  getFields,
  getTypeField,
  getValueFields,
  closest,
  getValidationAttributes,
  changeActiveFieldset,
} from '../src/domUtils';

test('Get attributes from form', () => {
  const form = document.createElement('form');
  form.setAttribute('action', 'handler.php');
  form.setAttribute('accept-charset', 'windows-1251');
  form.setAttribute('method', 'POST');
  form.setAttribute('enctype', 'multipart/form-data');

  const form2 = document.createElement('form');
  form2.setAttribute('action', 'foo.php');

  expect(getFormAttributes(form)).toEqual({
    action: 'handler.php',
    'accept-charset': 'windows-1251',
    method: 'POST',
    enctype: 'multipart/form-data',
  });

  expect(getFormAttributes(form2)).toEqual({
    action: 'foo.php',
    'accept-charset': 'utf-8',
    method: 'GET',
    enctype: 'application/x-www-form-urlencoded',
  });
});

test('Get form fieldsets', () => {
  const form = document.createElement('form');

  const fieldset = document.createElement('fieldset');
  const fieldset2 = document.createElement('fieldset');

  form.appendChild(fieldset);
  form.appendChild(fieldset2);

  expect(getFormFieldsets(form)).toEqual([fieldset, fieldset2]);
});

test('Get form fields', () => {
  const form = document.createElement('form');
  form.innerHTML = `
    <fieldset>
      <legend>Legend</legend>
      <input type="text" name="name">
      <input type="checkbox" name="consent">
      <input type="checkbox" name="consent">
    </fieldset>
    <input type="reset">
    <input type="image">
    <input type="hidden" name="hidden">
    <button type="button">Button</button>
    <button>Button</button>
    <input type="submit" value="Submit">
  `;

  const inputText = document.createElement('input');
  inputText.type = 'text';
  inputText.name = 'name';

  const inputTypeCheckBox = document.createElement('input');
  inputTypeCheckBox.type = 'checkbox';
  inputTypeCheckBox.name = 'consent';

  const hiddenField = document.createElement('input');
  hiddenField.type = 'hidden';
  hiddenField.name = 'hidden';

  const expected = {
    name: [inputText],
    consent: [inputTypeCheckBox, inputTypeCheckBox],
    hidden: [hiddenField],
  };

  expect(getFields(form)).toEqual(expected);
});


test('Get type field', () => {
  const textField = document.createElement('input');
  textField.type = 'text';

  const numberField = document.createElement('input');
  numberField.type = 'number';

  const checkboxField = document.createElement('input');
  checkboxField.type = 'checkbox';

  const radioField = document.createElement('input');
  radioField.type = 'radio';

  const selectOne = document.createElement('select');

  const selectMultiple = document.createElement('select');
  selectMultiple.multiple = 'multiple';

  const textarea = document.createElement('textarea');

  expect(getTypeField(textField)).toBe('text');
  expect(getTypeField(selectOne)).toBe('select');
  expect(getTypeField(selectMultiple)).toBe('select');
  expect(getTypeField(textarea)).toBe('text');
  expect(getTypeField(numberField)).toBe('number');
  expect(getTypeField(checkboxField)).toBe('checkbox');
  expect(getTypeField(radioField)).toBe('radio');
});

test('Get value fields', () => {
  const textField = document.createElement('input');
  textField.type = 'text';
  textField.value = 'ololo';

  const textFieldEmpty = textField.cloneNode(true);
  textFieldEmpty.value = '';

  const numberField = document.createElement('input');
  numberField.type = 'number';
  numberField.value = 56;

  const numberFieldEmpty = numberField.cloneNode(true);
  numberFieldEmpty.value = '';

  const checkbox = document.createElement('input');
  checkbox.value = 'Anna';
  checkbox.checked = true;
  const checkbox2 = checkbox.cloneNode(true);
  checkbox2.value = 'Petr';
  const checkbox3 = document.createElement('input');
  checkbox3.value = 'Egor';

  const selectField = document.createElement('select');
  selectField.setAttribute('multiple', true);
  selectField.innerHTML = `
    <option selected>---</option>
    <option value="male" selected>male</option>
    <option value="female" selected>female</option>
  `;

  const selectFieldUnselect = document.createElement('select');
  selectFieldUnselect.innerHTML = `
    <option>---</option>
    <option value="male">male</option>
    <option value="female">female</option>
  `;

  expect(getValueFields([textField], 'text')).toBe('ololo');
  expect(getValueFields([textFieldEmpty], 'text')).toBe(null);

  expect(getValueFields([numberField], 'number')).toBe(56);
  expect(getValueFields([numberFieldEmpty], 'number')).toBe(null);

  expect(getValueFields([checkbox, checkbox2, checkbox3], 'checkbox')).toEqual(['Anna', 'Petr']);
  expect(getValueFields([checkbox3, checkbox3], 'checkbox')).toBe(null);

  expect(getValueFields([selectField], 'select')).toEqual(['male', 'female']);
  expect(getValueFields([selectFieldUnselect], 'select')).toBe(null);
});

test('Closest', () => {
  const container = document.createElement('div');
  container.classList.add('container');

  const inner = document.createElement('div');
  inner.classList.add('inner');

  const input = document.createElement('input');
  input.type = 'radio';
  input.name = 'name';
  input.id = 'name-1';

  container.appendChild(inner);
  inner.appendChild(input);

  expect(closest(input, '.container')).toEqual(container);
});

test('Get validation attributes from element', () => {
  const inputText = document.createElement('input');
  inputText.type = 'text';
  inputText.setAttribute('minlength', 3);
  inputText.setAttribute('maxlength', 10);
  inputText.setAttribute('required', 'required');
  inputText.setAttribute('pattern', '2-[0-9]{3}-[0-9]{3}');
  inputText.setAttribute('data-equalTo', 'name');

  const inputNumber = document.createElement('input');
  inputNumber.type = 'number';
  inputNumber.setAttribute('min', 14);
  inputNumber.setAttribute('max', 35);

  const expectedRulesInputText = {
    minlength: 3,
    maxlength: 10,
    required: true,
    pattern: /2-[0-9]{3}-[0-9]{3}/,
    equalTo: 'name',
  };

  const expectedRulesInputNumber = {
    min: 14,
    max: 35,
  };

  expect(getValidationAttributes(inputText)).toEqual(expectedRulesInputText);
  expect(getValidationAttributes(inputNumber)).toEqual(expectedRulesInputNumber);
});

test('Change active fieldset', () => {
  const fieldset = document.createElement('fieldset');
  fieldset.classList.add(CLASS_NAMES.active, CLASS_NAMES.fromPrev);

  const fieldset2 = document.createElement('fieldset');
  fieldset2.classList.add(CLASS_NAMES.toNext);

  const fieldset3 = document.createElement('fieldset');
  fieldset3.classList.add(CLASS_NAMES.toPrev);

  const fieldset4 = document.createElement('fieldset');
  fieldset4.classList.add(CLASS_NAMES.active, CLASS_NAMES.fromNext);

  const fieldset5 = document.createElement('fieldset');
  fieldset5.classList.add(CLASS_NAMES.toPrev);

  const fieldset6 = document.createElement('fieldset');
  fieldset6.classList.add(CLASS_NAMES.active, CLASS_NAMES.fromNext);

  const fieldset7 = document.createElement('fieldset');
  fieldset7.classList.add(CLASS_NAMES.active, CLASS_NAMES.fromPrev);

  const fieldset8 = document.createElement('fieldset');
  fieldset8.classList.add(CLASS_NAMES.toNext);

  changeActiveFieldset([
    { DOM: fieldset },
    { DOM: fieldset2 },
  ], { dir: 'next', id: 0 });
  expect([fieldset, fieldset2]).toEqual([fieldset3, fieldset4]);

  changeActiveFieldset([
    { DOM: fieldset5 },
    { DOM: fieldset6 },
  ], { dir: 'prev', id: 1 });
  expect([fieldset5, fieldset6]).toEqual([fieldset7, fieldset8]);
});
