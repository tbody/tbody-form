import {
  CLASS_NAMES,
  DIR_CHANGE_FIELDSETS,
} from './constants';

import { hasProp, formatBytesToString } from './utils';

/**
 * @param {HTMLElement} form
 * @returns {object}
 * */
export const getFormAttributes = function getFromAttributes(form) {
  return {
    action: form.getAttribute('action'),
    'accept-charset': form.getAttribute('accept-charset') || 'utf-8',
    method: form.getAttribute('method') || 'GET',
  };
};

/**
 * @param {HTMLElement} form
 * @returns {array} Array of fieldsets nodes
 * */
export const getFormFieldsets = function getFormFieldsets(form) {
  return [...form.getElementsByTagName('fieldset')];
};

/**
 * @param {HTMLElement} parent
 * @returns {array} Array form fields (exclude all buttons and hidden fields)
 * */
export const getFields = function getFields(parent) {
  const forbiddenFieldType = ['submit', 'button', 'image', 'reset', 'fieldset'];

  return [...parent.querySelectorAll('input, select, textarea')]
    .filter(item => (
      forbiddenFieldType.indexOf(item.type) === -1
    ))
    .reduce((acc, item) => {
      const name = item.name;

      if (!hasProp(acc, name)) {
        acc[name] = [];
      }

      acc[name] = [...acc[name], item];
      return acc;
    }, {});
};

/**
 * Return type form element
 *
 * @param {HTMLElement} field - The form element
 * @returns {string} Type form element
 * */
export const getTypeField = function getTypeField(field) {
  const attrType = field.type;
  let type = null;

  switch (attrType) {
    case 'text':
    case 'email':
    case 'password':
    case 'textarea':
    case 'tel':
      type = 'text';
      break;
    case 'number':
    case 'checkbox':
    case 'radio':
    case 'file':
    case 'hidden':
      type = attrType;
      break;
    default:
      break;
  }

  if (attrType.indexOf('select') !== -1) {
    type = 'select';
  }

  return type;
};

export const getValueTextFields = function getValueTextFields(fields) {
  return fields[0].value.length ? fields[0].value : null;
};

export const getValueNumberFields = function getValueNumberField(fields) {
  return fields[0].value.length ? Number(fields[0].value) : null;
};

export const getValueCheckBoxFields = function getValueCheckBoxFields(fields) {
  const result = fields.reduce((acc, item) => {
    if (item.checked) {
      return [...acc, item.value];
    }
    return acc;
  }, []);

  return result.length ? result : null;
};

export const getValueRadioFields = function getValueRadioFields(fields) {
  return fields.reduce((acc, item) => {
    if (item.checked) {
      return item.value;
    }
    return acc;
  }, null);
};

export const getValueSelectFields = function getValueSelectFields(fields) {
  const result = [...fields[0].options].reduce((acc, item) => {
    const isSelectedAndNotEmpty = item.selected && item.getAttribute('value') !== null;
    if (isSelectedAndNotEmpty) {
      return [...acc, item.value];
    }
    return acc;
  }, []);

  return result.length ? result : null;
};

export const getValueFileFields = function getValueFileFields(fields) {
  const result = fields.reduce((acc, item) => {
    if (item.files.length) {
      return [...acc, ...item.files];
    }
    return acc;
  }, []);

  return result.length ? result : null;
};

const mapGetValueMethods = {
  text: getValueTextFields,
  number: getValueNumberFields,
  radio: getValueRadioFields,
  checkbox: getValueCheckBoxFields,
  select: getValueSelectFields,
  file: getValueFileFields,
  hidden: getValueTextFields,
};

/**
 * @param {array} fields
 * @param {string} type
 * @returns {string, number} value
 * */
export const getValueFields = function getValueField(fields, type) {
  return mapGetValueMethods[type](fields);
};


/**
 * Add event listeners to array DOM elements.
 *
 * @param {array} elements - The DOM elements.
 * @param {array} events - The DOM events.
 * @param {function} handler - The handler function to DOM events.
 * */
export const addListeners = function addEventListener(elements, events, handler) {
  elements.forEach(element => (
    events.forEach(eventName => (
      element.addEventListener(eventName, handler)
    ))
  ));
};

/**
 * @param {HTMLElement} element
 * @param {string} selector
 * */
export const closest = function closest(element, selector) {
  let el = element;
  const matchesSelector = el.matches ||
    el.webkitMatchesSelector ||
    el.mozMatchesSelector ||
    el.msMatchesSelector;

  while (el) {
    if (matchesSelector.call(el, selector)) {
      break;
    }
    el = el.parentElement;
  }
  return el;
};

/**
 * Get validation attributes from HTMLElement
 *
 * @param {HTMLElement} elem
 * @returns {object}
 * */
export const getValidationAttributes = function getValidationAttributes(elem) {
  return Array.prototype.slice.call(elem.attributes).reduce((acc, item) => {
    const newAcc = acc;
    const name = item.name.toLowerCase();
    const value = item.value;

    switch (name) {
      case 'min':
        newAcc.min = Number(value);
        break;
      case 'max':
        newAcc.max = Number(value);
        break;
      case 'data-minlength':
      case 'minlength':
        newAcc.minlength = Number(value);
        break;
      case 'data-maxlength':
      case 'maxlength':
        newAcc.maxlength = Number(value);
        break;
      case 'required':
        newAcc.required = true;
        break;
      case 'pattern':
        newAcc.pattern = new RegExp(value);
        break;
      case 'data-equalto':
        newAcc.equalTo = value;
        break;
      case 'data-accept':
        newAcc.accept = value;
        break;
      case 'data-minfilesize':
        newAcc.minfilesize = Number(value);
        break;
      case 'data-maxfilesize':
        newAcc.maxfilesize = Number(value);
        break;
      case 'data-mincount':
        newAcc.mincount = Number(value);
        break;
      case 'data-maxcount':
        newAcc.maxcount = Number(value);
        break;
      default:
        break;
    }

    return newAcc;
  }, {});
};

/**
 * @param {array} fieldsets
 * @param {string} dir - The direction change fieldset - "prev" or "next"
 * @param {string} id - The id current fieldset
 * */
export const changeActiveFieldset = function changeActiveFieldset(fieldsets, { dir, id }) {
  fieldsets.forEach((item) => {
    item.DOM.classList.remove(
      CLASS_NAMES.active,
      CLASS_NAMES.fromPrev,
      CLASS_NAMES.toPrev,
      CLASS_NAMES.fromNext,
      CLASS_NAMES.toNext,
    );
  });

  let nextId = null;
  let classCurrentFieldset = null;
  let classNextFieldset = null;

  if (dir === DIR_CHANGE_FIELDSETS.prev) {
    nextId = id - 1;
    classCurrentFieldset = CLASS_NAMES.toNext;
    classNextFieldset = CLASS_NAMES.fromPrev;
  } else if (dir === DIR_CHANGE_FIELDSETS.next) {
    nextId = id + 1;
    classCurrentFieldset = CLASS_NAMES.toPrev;
    classNextFieldset = CLASS_NAMES.fromNext;
  }

  fieldsets[id].DOM.classList.add(classCurrentFieldset);
  fieldsets[nextId].DOM.classList.add(CLASS_NAMES.active, classNextFieldset);
};

/**
 * @param {string} id
 * @param {File} file
 * @returns {HTMLElement}
 * */
export const createFilePreview = function createFilePreview(id, file) {
  const preview = document.createElement('div');
  preview.classList.add(CLASS_NAMES.filePreview);
  preview.setAttribute('data-id', id);

  const close = document.createElement('button');
  close.classList.add(CLASS_NAMES.close);
  close.textContent = '×';
  preview.appendChild(close);


  const isImg = file.type.indexOf('image') !== -1;
  let child = null;

  if (isImg) {
    child = document.createElement('img');
    child.src = URL.createObjectURL(file);
  } else {
    child = document.createElement('div');
  }

  const typeFile = file.type.replace(/\//, '-') || 'unknown';
  child.classList.add(typeFile);
  preview.appendChild(child);

  const fileName = document.createElement('div');
  fileName.classList.add(CLASS_NAMES.fileName);
  fileName.innerText = file.name;
  preview.appendChild(fileName);

  const fileSize = document.createElement('div');
  fileSize.classList.add(CLASS_NAMES.fileSize);
  fileSize.innerText = formatBytesToString(file.size);
  preview.appendChild(fileSize);


  return preview;
};
