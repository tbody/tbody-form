import {
  CLASS_NAMES,
} from './constants';

import {
  closest,
} from './domUtils';

/**
 * Create DOM element error message
 * @param {HTMLElement} field
 * @param {string} text
 * @returns {HTMLElement} - The HTMLElement error message
 * */
export const createError = function createError(field, text) {
  const id = field.getAttribute('id');

  const error = document.createElement('label');
  error.setAttribute('for', id);
  error.textContent = text;
  error.classList.add(CLASS_NAMES.error);

  return error;
};

/**
 * @param {HTMLElement} field
 * @param {string} text
 * @param {HTMLElement} errorElement
 * @param {HTMLElement} parentNode
 * */
export const showError = function showError(field, text, errorElement, parentNode) {
  const parent = parentNode ||
    closest(field, `.${CLASS_NAMES.fieldContainer}`) ||
    field.parentNode;

  if (errorElement) {
    const error = errorElement;
    error.textContent = text;
    error.removeAttribute('style');
    return {
      error,
      parent,
    };
  }

  const error = createError(field, text);
  parent.appendChild(error);
  parent.classList.add(CLASS_NAMES.errorContainer);

  return {
    error,
    parent,
  };
};

/**
 * @param {HTMLElement} errorElement
 * @param {HTMLElement} parentNode
 * */
export const hideError = function hideError(errorElement, parentNode) {
  if (errorElement) {
    const error = errorElement;
    error.style.display = 'none';
    parentNode.classList.remove(CLASS_NAMES.errorContainer);
    return {
      error,
      parent: parentNode,
    };
  }

  return null;
};

/**
 * Show or hide field error
 * Store error element inside scope
 * */
export const toggleFieldError = (function toggleFieldError() {
  const errors = {};
  const errorContainers = {};

  /**
   * @param {array} fields - Array fields DOM elements
   * @param {string} type - 'show' or 'hide' error message
   * @param {string} text - Text error message
   * */
  return function innerToggleFieldError(fields, type, text) {
    const name = fields[0].name;
    let result = null;

    if (type === 'show') {
      result = showError(fields[0], text, errors[name], errorContainers[name]);
    } else {
      result = hideError(errors[name], errorContainers[name]);
    }

    if (result) {
      errors[name] = result.error;
      errorContainers[name] = result.parent;
    }
  };
}());
