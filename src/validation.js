import {
  CLASS_NAMES,
} from './constants';

import {
  isNumberOrString,
  isArraysToEqual,
  filterArrayOfObjectsByKey,
  createFileUniqueId,
} from './utils';

/**
 * Change status validation from DOM element form or fieldset.
 * Add and delete CSS class
 *
 * @param {array} children
 * */
export const getStatusValidationFields = function getStatusValidationFields(children) {
  return children
    .map(item => item.getStatus())
    .every(item => item);
};

/**
 * Change status validation from DOM element.
 * Add and delete CSS class
 *
 * @param {array} nodes
 * @param {boolean} isValid
 * */
export const changeStatusValidation = function changeStatusValidation(nodes, isValid) {
  const classToRemove = isValid ? CLASS_NAMES.nonvalid : CLASS_NAMES.valid;
  const classToAdd = isValid ? CLASS_NAMES.valid : CLASS_NAMES.nonvalid;

  nodes.forEach(node => node.classList.remove(classToRemove));
  nodes.forEach(node => node.classList.add(classToAdd));
};


/**
 * Validation by empty string or number value, empty array
 *
 * @param {*} value
 * @return {object} Result check.
 * */
export const validateRequired = function isEmpty(value) {
  let val = value;

  /* 0 is valid value number field and so it need convert to string
  for to transform true
  */
  if (value === 0) {
    val = String(value);
  }

  return {
    type: 'required',
    isValid: Boolean(val),
  };
};

/**
 * Check length string value or array
 *
 * @param {string} type
 * @param {*} value
 * @param {number} rule
 * @return {object} Result check.
 * */
export const validateLength = function validateLength(type, value, rule) {
  const isNotArrayOrString = !Array.isArray(value) && typeof value !== 'string';
  if (isNotArrayOrString) {
    throw new TypeError('This value is not array or string');
  }
  let isValid = null;

  switch (type) {
    case 'minlength':
      isValid = value.length >= rule;
      break;
    case 'maxlength':
      isValid = value.length <= rule;
      break;
    default:
      break;
  }

  return {
    isValid,
    type,
  };
};

/**
 * Check string by regexp
 *
 * @param {*} value
 * @param {RegExp} pattern
 * @return {object} Result check.
 * */
export const validateByPattern = function validateByPattern(value, pattern) {
  const isNotString = typeof value !== 'string';
  if (isNotString) {
    throw new TypeError('This value is not string');
  }

  return {
    type: 'pattern',
    isValid: pattern.test(value),
  };
};

/**
 * Check number value
 *
 * @param {string} type - 'min' or 'max'
 * @param {*} value
 * @param {number} rule
 * @return {object} Result check.
 * */
export const validateNumber = function validateNumber(type, value, rule) {
  if (typeof value !== 'number' || isNaN(value)) {
    throw new TypeError('This value is not number');
  }

  let isValid = null;

  switch (type) {
    case 'min':
      isValid = value >= rule;
      break;
    case 'max':
      isValid = value <= rule;
      break;
    default:
      break;
  }

  return {
    isValid,
    type,
  };
};


/**
 * Compare two values
 *
 * @param {*} value
 * @param {*} standard
 * @return {object} Result check.
 * */
export const validateEqualTo = function validateEqualTo(value, standard) {
  let isValid;

  if (isNumberOrString(value) && isNumberOrString(standard)) {
    isValid = value === standard;
  } else if (Array.isArray(value) && Array.isArray(standard)) {
    isValid = isArraysToEqual(value, standard);
  } else if (value === null || standard === null) {
    isValid = false;
  } else {
    throw new TypeError('It not admissible comparison');
  }

  return {
    isValid,
    type: 'equalTo',
  };
};

/**
 * Delegate validation file fields
 *
 * @param {string} type - Type validation - 'accept', 'minfilezize', 'maxfilesize'
 * @param {array} value - The array Files
 * @param {number} rule - The validation rule
 * @return {object} - Return status of validation, type error and nonvalid files
 * */
export const validateFiles = function validateFiles(type, value, rule) {
  const errorFiles = [];

  const isValid = value
    .map((item) => {
      let isFileValid = true;
      switch (type) {
        case 'accept':
          isFileValid = item.type || rule.indexOf(item.type) !== -1;
          break;
        case 'minfilesize':
          isFileValid = item.size >= rule;
          break;
        case 'maxfilesize':
          isFileValid = item.size <= rule;
          break;
        default:
          break;
      }

      if (!isFileValid) {
        const id = createFileUniqueId(item);
        errorFiles.push(id);
      }
      return isFileValid;
    })
    .every(item => item);

  return {
    isValid,
    type,
    errorFiles: errorFiles.length ? errorFiles : null,
  };
};


const orderValidation = [
  {
    type: 'required',
    method: validateRequired,
  },
  {
    type: 'pattern',
    method: validateByPattern,
  },
  {
    type: 'accept',
    method: validateFiles.bind(null, 'accept'),
  },
  {
    type: 'minfilesize',
    method: validateFiles.bind(null, 'minfilesize'),
  },
  {
    type: 'maxfilesize',
    method: validateFiles.bind(null, 'maxfilesize'),
  },
  {
    type: 'minlength',
    method: validateLength.bind(null, 'minlength'),
  },
  {
    type: 'maxlength',
    method: validateLength.bind(null, 'maxlength'),
  },
  {
    type: 'min',
    method: validateNumber.bind(null, 'min'),
  },
  {
    type: 'max',
    method: validateNumber.bind(null, 'max'),
  },
  {
    type: 'equalTo',
    method: validateEqualTo,
  },
];

/**
 * Main validation function. Run actual validation methods and return result validation
 *
 * @param {object} rules - The object contains name of validation methods and rules validation
 * @param {*} value - Source value
 * @return {object} - prop "type" - type error, prop "valid" - status validation
 * */
const validateByMethodsNames = function validateByMethodsNames(rules, value) {
  const actualMethods = filterArrayOfObjectsByKey(orderValidation, rules, 'type');
  const resultValidation = {};

  resultValidation.isValid = actualMethods.every((item) => {
    const result = item.method(value, rules[item.type]);
    if (!result.isValid) {
      resultValidation.type = result.type;
    }
    if (result.errorFiles) {
      resultValidation.errorFiles = result.errorFiles;
    }
    return result.isValid;
  });

  return resultValidation;
};

export default validateByMethodsNames;
