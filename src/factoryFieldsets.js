import {
  CLASS_NAMES,
  EVENTS_NAMES,
} from './constants';

import Fieldset from './constructors/Fieldset';
import { getFormFieldsets } from './domUtils';

/**
 * @param {object} parent
 * @param {object} messages
 * @returns {array}
 * */
const factoryFieldsets = function factoryFieldsets(parent, messages) {
  const fieldsetsDOM = getFormFieldsets(parent.DOM);

  return fieldsetsDOM.map((item, i) => {
    const fieldset = new Fieldset(i, item, messages);

    fieldset.init();
    if (i === 0) {
      fieldset.DOM.classList.add(CLASS_NAMES.active);
    }

    parent.subscribe(fieldset.validate, EVENTS_NAMES.validate);
    parent.subscribe(fieldset.getValue, EVENTS_NAMES.getValue);

    return fieldset;
  });
};

export default factoryFieldsets;
