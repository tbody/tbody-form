import {
  EVENTS_NAMES,
} from './constants';

import { subscribeToEqualField } from './utils';

import {
  getFields,
  getTypeField,
  getValidationAttributes,
} from './domUtils';

import BaseField from './constructors/BaseField';
import FileField from './constructors/FileField';
import HiddenField from './constructors/HiddenField';

const fieldsMapConstructors = {
  text: BaseField,
  number: BaseField,
  select: BaseField,
  radio: BaseField,
  checkbox: BaseField,
  file: FileField,
  hidden: HiddenField,
};

/**
 * Create instances objects fields
 * @param {HTMLElement} parent - The array HTMLElement form
 * @param {object} messages - Error messages
 * @returns {array} - The array instances objects fields
 * */
export default function factoryFields(parent, messages) {
  const fieldsDOM = getFields(parent.DOM);

  const fields = Object.keys(fieldsDOM).map((name, index) => {
    const fieldElement = fieldsDOM[name][0];
    const type = getTypeField(fieldElement);

    const settings = {
      rules: getValidationAttributes(fieldElement),
      messages: messages[name],
    };

    const field = new fieldsMapConstructors[type](
      index,
      name,
      fieldsDOM[name],
      type,
      settings,
    );

    parent.subscribe(field.validate, EVENTS_NAMES.validate);
    parent.subscribe(field.getValue, EVENTS_NAMES.getValue);

    return field;
  });

  subscribeToEqualField(fields);

  return fields;
}
