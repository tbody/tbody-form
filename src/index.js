import {
  EVENTS_NAMES,
} from './constants';

import GroupFields from './constructors/GroupFields';
import factoryFields from './factoryFields';
import factoryFieldsets from './factoryFieldsets';

import {
  isFunction,
  subscribeOneToMany,
  sendAJAX,
  createFormData,
} from './utils';

import {
  changeActiveFieldset,
  getFormAttributes,
} from './domUtils';

/**
 * Class representing a form.
 * */
class Tbodyform extends GroupFields {
  /**
   * Create a form
   *
   * @param {HTMLElement} node - The node form
   * @param {boolean} isMultistep
   * @param {object} messages
   * @param {function} dataFn
   * @param {function} beforeSend
   * @param {function} successCb
   * @param {function} errorCb
   * */
  constructor({ node, messages, isMultistep, dataFn, beforeSend, successCb, errorCb }) {
    super(0, node);
    this.DOM = node;
    this.submit = null;
    this.messages = messages;

    this.dataFn = dataFn;
    this.beforeSend = beforeSend;
    this.successCb = successCb;
    this.errorCb = errorCb;

    this.changeFieldset = this.changeFieldset.bind(this);

    if (isMultistep) {
      this.children = factoryFieldsets(this, this.messages);
      subscribeOneToMany(this.changeFieldset, this.children, EVENTS_NAMES.changeFieldset);
    } else {
      this.children = factoryFields(this, this.messages);
    }
  }

  findControls() {
    this.submit = this.DOM.elements.submit;
  }

  addListenersEvents() {
    this.submit.addEventListener('click', this.handleValidation);
  }

  successValidationHandler() {
    const isHasCbBeforeSend = this.beforeSend && {}.toString.call(this.beforeSend) === '[object Function]';
    if (isHasCbBeforeSend) {
      this.beforeSend(this);
    }

    this.send();
  }

  changeFieldset(param) {
    changeActiveFieldset(this.children, param);
  }

  send() {
    // Flat array
    const data = this.getValue().reduce((acc, item) => acc.concat(item), []);
    const hasDataFn = isFunction(this.dataFn);
    const userData = hasDataFn ? this.dataFn(this) : [];
    const formData = createFormData([...data, ...userData]);

    const params = getFormAttributes(this.DOM);

    sendAJAX(this, params, formData, this.successCb, this.errorCb);
  }
}

export default Tbodyform;
