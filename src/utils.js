import { EVENTS_NAMES } from './constants';

/**
 * Check value is string or number
 *
 * @param {*} value
 * @return {boolean} Result check
 * */
export const isNumberOrString = function isNumberOrString(value) {
  return (
    (typeof value === 'number' && !isNaN(value)) || typeof value === 'string'
  );
};

/**
 * Check has object property
 *
 * @param {object} obj - Source object
 * @param {number, string} prop - Property
 * @return {boolean} Result check
 * */

export const hasProp = function hasProp(obj, prop) {
  if (obj === undefined || !isNumberOrString(prop) || obj === null) {
    return false;
  }
  return Object.prototype.hasOwnProperty.call(obj, prop);
};

export const isFunction = function isFunction(obj) {
  return !!(obj && obj.constructor && obj.call && obj.apply);
};


/**
 * Compare two flat array
 *
 * @param {array} arr1
 * @param {array} arr2
 * @return {boolean} Result check
 * */
export const isArraysToEqual = function isArraysToEqual(arr1, arr2) {
  if (arr1.length !== arr2.length) {
    return false;
  }

  for (let i = 0; i < arr1.length; i += 1) {
    if (!isNumberOrString(arr1[i]) || !isNumberOrString(arr2[i])) {
      throw new TypeError('It not admissible comparison');
    }
    if (arr1[i] !== arr2[i]) {
      return false;
    }
  }

  return true;
};

/**
 * @param {array} arr - Source Array of objects
 * @param {object} obj - Target object
 * @param {string} key - Name object prop
 * @return {array} Filtered array of object
 * */
export const filterArrayOfObjectsByKey = function filterArrayOfObjectsByKey(arr, obj, key) {
  return arr.filter(item => hasProp(obj, item[key]));
};

/**
 * @param {function} fn
 * @param {array} observables
 * @param {string} type
 * */
export const subscribeOneToMany = function subscribeOneToMany(fn, observables, type) {
  observables.forEach(observable => observable.subscribe(fn, type));
};

/**
 * @param {array} fields
 * */
export const subscribeToEqualField = function subscribeToEqualField(fields) {
  fields.forEach((field) => {
    if (hasProp(field, 'rules') && hasProp(field.rules, 'equalTo')) {
      const targetField = fields.filter(item => item.name === field.rules.equalTo)[0];
      targetField.subscribe(field.setValueEqualField, EVENTS_NAMES.changeValue);
    }
  });
};

/**
 * @param {File} file
 * @retursn {string}
 * */
export const createFileUniqueId = function createFileUniqueId(file) {
  return file.lastModified + file.name + file.size;
};

/**
 * @param {object} params - Contain XMLHTTPRequest settings: method, url, enctype, charset
 * @param {FormData} formData
 * @param {function} successCb - The callback for success send ajax
 * @param {function} errorCb - The callback for error send ajax
 * */
export const sendAJAX = function sendAJAX(form, params, formData, successCb, errorCb) {
  const xhr = new XMLHttpRequest();

  xhr.open(params.method, params.action, true);
  xhr.send(formData);

  xhr.onreadystatechange = function handleReadeStateChange() {
    const hasError = this.readyState !== 4 || this.status !== 200;

    if (hasError) {
      return false;
    }

    if (isFunction(errorCb) && hasError) {
      errorCb(form, xhr.status, xhr.statusText);
    }

    if (isFunction(successCb) && !hasError) {
      successCb(form, xhr.responseText);
    }
    return false;
  };
};

/**
 * Format sting bytes to category
 *
 * @param {string, number} bytes
 * @returns {string}
 * @example
 * 14000 => 13.67KB
 * 4304500 => 4.11MB
 * 8324730400 => 7.85GB
 * const result
 * */
export const formatBytesToString = function formatBytesToString(bytes) {
  const basis = 1024;
  const suffixes = ['KB', 'MB', 'GB'];
  let value = Number(bytes);
  let result = null;

  for (let i = 0; i < suffixes.length; i += 1) {
    value /= basis;
    if (value < basis || i === suffixes.length - 1) {
      result = `${value.toFixed(2)} ${suffixes[i]}`;
      break;
    }
  }

  return result;
};

/**
 * Create instance FormData
 *
 * @param {array} fields
 * @returns {FormData}
 **/
export const createFormData = function createFormData(fields) {
  return fields.reduce((acc, item) => {
    if (Array.isArray(item.value)) {
      item.value.forEach(val => acc.append(item.name, val));
    } else {
      acc.append(item.name, item.value);
    }

    return acc;
  }, new FormData());
};
