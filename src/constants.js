export const CLASS_NAMES = {
  active: 'js-active',
  valid: 'js-valid',
  nonvalid: 'js-nonvalid',
  hasError: 'js-has-error',
  error: 'js-error',
  errorContainer: 'js-has-error',
  fieldContainer: 'js-field-container',
  controlPrevFieldset: 'js-prev-fieldset',
  controlNextFieldset: 'js-next-fieldset',
  fromPrev: 'js-from-prev',
  toPrev: 'js-to-prev',
  fromNext: 'js-from-next',
  toNext: 'js-to-next',
  close: 'js-close',
  filePreview: 'js-file-preview',
  fileName: 'js-file-name',
  fileSize: 'js-file-size',
};

export const DIR_CHANGE_FIELDSETS = {
  prev: 'prev',
  next: 'next',
};

export const EVENTS_NAMES = {
  getValue: 'getValue',
  changeFieldset: 'changeFieldset',
  changeStatus: 'changeStatus',
  changeValue: 'changeValue',
  validate: 'validate',
};
