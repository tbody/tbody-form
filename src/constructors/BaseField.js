import Field from './Field';

import {
  getValueFields,
  addListeners,
} from '../domUtils';

/**
 * Class представляющий тектсовые поля, select, radio, checkbox
 * */
export default class BaseField extends Field {
  /**
   * @param {number} id - Индекс (порядковый номер) поля.
   * @param {string} name - Имя поля.
   * @param {array} nodes - Массив DOM элементов которые относятся к данному полу.
   * @param {string} type - Тип поля.
   * @param {object} rules - Правила валидации.
   * @param {object} messages - Сообщения об ошибках.
   * */
  constructor(id, name, nodes, type, { rules, messages }) {
    super(id, name, nodes, type, { rules, messages });

    // Названия DOM событий по которым будет запускаться валидация поля
    this.namesEventListeners = ['keyup', 'change', 'blur'];
    this.init();
  }

  /**
   * Инициализация поля
   * */
  init() {
    this.setValue();
    this.addListenersEvents();
  }


  /**
   * Устанавливает значение поля
   * Берет значение из DOM элемента(ов) и заносит это значение в свойство экземпляра класса
   * */
  setValue() {
    this.value = getValueFields(this.DOM, this.type);
  }

  /**
   * Добавляет прослушивание всех (указанных в настройках) DOM событий на все DOM элементы поля
   * */
  addListenersEvents() {
    addListeners(this.DOM, this.namesEventListeners, this.handleChange);
  }
}
