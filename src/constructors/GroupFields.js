import Observable from './Observable';
import { subscribeOneToMany } from '../utils';

import {
  EVENTS_NAMES,
} from '../constants';

import {
  changeStatusValidation,
} from '../validation';

/**
 * Class группы полей (form или fieldset)
 * */
class GroupFields extends Observable {
  /**
   * @param {number} id - Индекс (порядковый номер) группы.
   * @param {HTMLElement} node - DOM элемент группы
   * */
  constructor(id, node) {
    super();
    this.id = id;
    this.DOM = node;
    this.children = null;
    this.childrenStatus = [];

    this.handleValidation = this.handleValidation.bind(this);
    this.validate = this.validate.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    this.getValue = this.getValue.bind(this);
  }

  /**
   * Инициализация
   * */
  init() {
    this.findControls();
    this.addListenersEvents();
    subscribeOneToMany(this.changeStatus, this.children, EVENTS_NAMES.changeStatus);
  }

  /**
   * Обработчик события запуска валидации
   * */
  handleValidation(e) {
    e.preventDefault();
    const isValid = this.validate();
    if (isValid) {
      this.successValidationHandler();
    }
    return false;
  }

  /**
   * Изменяет статус валидации группы полей
   *
   * @param {string} id - Индекс (порядковый номер) вложенного поля.
   * @param {boolean} status - Статус валидации вложенного поля.
   * */
  changeStatus({ id, status }) {
    this.childrenStatus[id] = status;

    // Проверяет все ли вложенные поля валидны
    const isValid = this.childrenStatus.every(item => item);

    changeStatusValidation([this.DOM], isValid);
    this.publish({
      status: isValid,
      id: this.id,
    }, EVENTS_NAMES.changeStatus);
  }

  /**
   * Возвращает значения всех вложенных полей
   *
   * @return {array} - массив объектов содержащих название поля и значение
   * */
  getValue() {
    return this.publish(null, EVENTS_NAMES.getValue);
  }

  /**
   * Запуск валидации вложенных полей
   * */
  validate() {
    const isValid = this.publish(null, EVENTS_NAMES.validate).indexOf(false) === -1;
    changeStatusValidation([this.DOM], isValid);
    return isValid;
  }
}

export default GroupFields;
