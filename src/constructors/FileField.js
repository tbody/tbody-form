import Field from './Field';

import {
  CLASS_NAMES,
} from '../constants';

import {
  closest,
  getValueFields,
  createFilePreview,
  addListeners,
} from '../domUtils';

import {
  hasProp,
  createFileUniqueId,
} from '../utils';

import {
  changeStatusValidation,
} from '../validation';


/**
 * Class представляющий файловый тип поля
 * */
export default class FileField extends Field {
  /**
   * @param {number} id - Индекс (порядковый номер) поля.
   * @param {string} name - Имя поля.
   * @param {array} nodes - Массив DOM элементов которые относятся к данному полу.
   * @param {string} type - Тип поля.
   * @param {object} rules - Правила валидации.
   * @param {object} messages - Сообщения об ошибках.
   * */
  constructor(id, name, nodes, type, { rules, messages }) {
    super(id, name, nodes, type, { rules, messages });
    this.fileZone = null;
    this.previews = {};

    // Названия DOM событий по которым будет запускаться валидация поля
    this.namesEventListeners = ['change'];
    this.removePreview = this.removePreview.bind(this);
    this.init();
  }

  /**
   * Инициализация
   * */
  init() {
    this.setValue();
    this.addListenersEvents();
    this.fileZone = document.querySelector(this.DOM[0].getAttribute('data-filezone'));
  }

  /**
   * Удаляет превью файлов
   * */
  removePreview(e) {
    const target = e.target;

    // Если клик произошел по кнопке закрыть
    if (target.classList.contains(CLASS_NAMES.close)) {
      e.preventDefault();
      // Находит текущее превью файла и его id
      const preview = closest(target, `.${CLASS_NAMES.filePreview}`);
      const id = preview.getAttribute('data-id');

      // Удаляет DOM элемент текущего превью
      preview.parentNode.removeChild(preview);

      // Удаляет текущее превью из свойства экземпляра класса
      delete this.previews[id];
      // Удаляет значение текущего файла из свойства экземпляра класса
      this.removeValue(id);
      if (this.isDirty) {
        this.validate();
      }
    }
  }

  /**
   * Удаляет значение текущего файла из свойства экземпляра класса
   *
   * @param {string} id - Идентификатор превью файла
   * */
  removeValue(id) {
    this.value = this.value.filter(item => createFileUniqueId(item) !== id);
  }

  /**
   * Устанавливает значение поля
   * */
  setValue() {
    // Находит текущее значение DOM поля
    const value = getValueFields(this.DOM, this.type);
    const oldValue = this.value;

    /* Фильтрует все DOM элементы которые не имеют значения
     * как правило это поле имеет только один DOM элемент
     * это сделано для совместимости с классом Field
    */
    this.DOM.forEach(item => (item.value = '')); // eslint-disable-line no-param-reassign

    /* Если текущее значение null установить текущее значеник как пустой массив
     * это сделано для удобное добавления новых значений
     */
    if (!oldValue) {
      this.value = [];
    }

    /* Если было получено значение из DOM элемента
     * проходится по всем значениям и добавляет их
     * в свойство экземпляра класса и отсеивает дублирование
     * После добавления всех значений запускает функцию
     * вставки DOM элементов превью файлов
     * */
    if (value) {
      this.value = value.reduce((acc, item) => {
        if (acc.indexOf(item) === -1) {
          return [...acc, item];
        }
        return acc;
      }, this.value);
      this.insertPreview();
    }

    if (!value && (!oldValue || !oldValue.length)) {
      this.value = null;
    }
  }

  /**
   * Вставляет DOM элементы превью файла
   * */
  insertPreview() {
    const newFiles = this.value.filter((item) => {
      /* Уникальный id превью который содержит
      * имя файла, размер и дату последнего изменния
       */
      const id = createFileUniqueId(item);

      // Отсеивает уже существующие превью
      if (!hasProp(this.previews, id)) {
        return item;
      }
      return false;
    });

    // Фрагмент для групповой вставки DOM элементов
    const fragment = document.createDocumentFragment();
    const newPreviews = newFiles.reduce((acc, item) => {
      const id = createFileUniqueId(item);
      const preview = createFilePreview(id, item);
      this.previews[id] = preview;
      acc.appendChild(preview);
      return acc;
    }, fragment);

    this.fileZone.appendChild(newPreviews);
  }

  /**
   * Изменяет статус превью файлов
   *
   * @param {object} errorFiles - Файлы которые не прошли валидацию
   * */
  changeStatusFilePreview(errorFiles) {
    const errorPreview = Object.keys(this.previews).reduce((acc, item) => {
      const hasError = errorFiles.indexOf(item) !== -1;
      if (hasError) {
        return [...acc, this.previews[item]];
      }
      return acc;
    }, []);

    changeStatusValidation(errorPreview, false);
  }

  /**
   * Добавляет прослушивание всех (указанных в настройках) DOM событий на все DOM элементы поля
   * и кнокпи удаления превью файлов
   * */
  addListenersEvents() {
    addListeners(this.DOM, this.namesEventListeners, this.handleChange);
    document.addEventListener('click', this.removePreview);
  }
}
