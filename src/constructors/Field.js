import Observable from './Observable';

import {
  EVENTS_NAMES,
} from '../constants';

import validateByMethodsNames, { changeStatusValidation } from '../validation';

import {
  toggleFieldError,
} from '../errors';

/**
 * Class представляющий базовое поле от которого
 * наследуются другие поля
 * */
export default class Field extends Observable {
  /**
   * @param {number} id - Индекс (порядковый номер) поля.
   * @param {string} name - Имя поля.
   * @param {array} nodes - Массив DOM элементов которые относятся к данному полу.
   * @param {string} type - Тип поля.
   * @param {object} rules - Правила валидации.
   * @param {object} messages - Сообщения об ошибках.
   * */
  constructor(id, name, nodes, type, { rules, messages }) {
    super();
    this.id = id;
    this.name = name;
    this.DOM = [].concat(nodes);
    this.type = type;
    this.rules = rules;
    this.messages = messages;
    this.value = null;
    this.isDirty = false;
    this.isValid = false;

    this.handleChange = this.handleChange.bind(this);
    this.setValueEqualField = this.setValueEqualField.bind(this);
    this.validate = this.validate.bind(this);
    this.getValue = this.getValue.bind(this);
  }


  /**
   * Обработчик всех событий которые вызывают изменение value DOM элемента
   * */
  handleChange(e) {
    e.preventDefault();
    this.setValue();
    this.publish(this.value, EVENTS_NAMES.changeValue);

    if (this.isDirty) {
      this.validate();
    }
  }

  /**
   * Возвращает значение поля
   *
   * @return {object} - объект содержащий название поля и значение
   * */
  getValue() {
    this.setValue();
    return {
      name: this.name,
      value: this.value,
    };
  }

  /**
   * Валидация поля
   * */
  validate() {
    this.isDirty = true;
    this.setValue();

    const result = validateByMethodsNames(this.rules, this.value);
    const isValid = result.isValid;
    this.isValid = isValid;

    this.publish({
      id: this.id,
      status: result.isValid,
    }, EVENTS_NAMES.changeStatus);

    const errorMessage = isValid ? null : this.messages[result.type];
    const actionError = isValid ? 'hide' : 'show';

    if (this.type === 'file' && result.errorFiles) {
      this.changeStatusFilePreview(result.errorFiles);
    }

    changeStatusValidation(this.DOM, result.isValid);
    toggleFieldError(this.DOM, actionError, errorMessage);
    return result.isValid;
  }

  /**
   * Устанавливает значения эквивалентного поля (правило equalto)
   * */
  setValueEqualField(value) {
    this.rules.equalTo = value;
    if (this.isDirty) {
      this.validate();
    }
  }
}
