import { hasProp } from '../utils';

/**
 * Class представляющий издателя событий
 * */
class Observable {
  constructor() {
    this.subscribers = {};
  }

  /**
   * Добавляет подписчиков
   *
   * @param {function} fn - Функция-подписчик
   * @param {string} type - Тип события на которое подписывается функция
   * */
  subscribe(fn, type) {
    if (!hasProp(this.subscribers, type)) {
      this.subscribers[type] = [];
    }
    this.subscribers[type].push(fn);
  }

  /**
   * Публикация события
   * @param {*} publication - Параметр который будет передан в функции подписчики
   * @param {string} type - Тип события
   * */
  publish(publication, type) {
    if (hasProp(this.subscribers, type)) {
      return this.subscribers[type].map(item => item(publication));
    }
    return null;
  }
}

export default Observable;
