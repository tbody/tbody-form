import Observable from './Observable';

import {
  getValueFields,
} from '../domUtils';


/**
 * Class представляющий тип поля hidden.
 * */
export default class HiddenField extends Observable {
  /**
   * @param {number} id - Индекс (порядковый номер) поля.
   * @param {string} name - Имя поля.
   * @param {array} nodes - Массив DOM элементов которые относятся к данному полу.
   * */
  constructor(id, name, nodes) {
    super();
    this.id = id;
    this.name = name;
    this.DOM = [].concat(nodes);
    this.type = 'hidden';
    this.value = null;

    this.validate = this.validate.bind(this);
    this.getValue = this.getValue.bind(this);

    this.init();
  }

  /**
   * Инициализация
   * */
  init() {
    this.setValue();
  }

  /**
   * Устанавливает значение поля
   * Берет значение из DOM элемента(ов) и заносит это значение в свойство экземпляра класса
   * */
  setValue() {
    this.value = getValueFields(this.DOM, this.type);
  }

  /**
   * Возвращает значение поля
   *
   * @return {object} - объект содержащий название поля и значение
   * */
  getValue() {
    this.setValue();
    return {
      name: this.name,
      value: this.value,
    };
  }

  /**
   * Валидация поля
   * данный метод необходим для совместимости с общей логикой программы
   * */
  validate() {
    this.setValue();

    return true;
  }
}
