import {
  CLASS_NAMES,
  DIR_CHANGE_FIELDSETS,
  EVENTS_NAMES,
} from '../constants';

import GroupFields from './GroupFields';
import factoryFields from '../factoryFields';

/**
 * Class представляющий fieldset.
 * */
export default class Fieldset extends GroupFields {
  /**
   * @param {number} id - Индекс (порядковый номер).
   * @param {HTMLElement} node - DOM элементы
   * @param {object} messages - Сообщения об ошибках
   * */
  constructor(id, node, messages) {
    super(id, node);
    this.controlNext = null;
    this.controlPrev = null;

    this.children = factoryFields(this, messages);

    this.handleClickPrevControl = this.handleClickPrevControl.bind(this);
  }

  /**
   * Находит кнопки переключения шагов
   * */
  findControls() {
    this.controlPrev = this.DOM.getElementsByClassName(CLASS_NAMES.controlPrevFieldset)[0];
    this.controlNext = this.DOM.getElementsByClassName(CLASS_NAMES.controlNextFieldset)[0];
  }

  /**
   * Добавляет прослушку событий клика по кнопкам переключения шагов
   * */
  addListenersEvents() {
    if (this.controlPrev) {
      this.controlPrev.addEventListener('click', this.handleClickPrevControl);
    }
    if (this.controlNext) {
      this.controlNext.addEventListener('click', this.handleValidation);
    }
  }

  /**
   * Обработчик клика по кнопку переключащий шаг на предыдущий
   * */
  handleClickPrevControl(e) {
    e.preventDefault();
    this.publish({
      dir: DIR_CHANGE_FIELDSETS.prev,
      id: this.id,
    }, EVENTS_NAMES.changeFieldset);
  }

  /**
   * Вызывается после клика на кнопку переключащей шаг на следующий
   * и успешного прохождения валидации всех полей
   * */
  successValidationHandler() {
    this.publish({
      dir: DIR_CHANGE_FIELDSETS.next,
      id: this.id,
    }, EVENTS_NAMES.changeFieldset);
  }
}
